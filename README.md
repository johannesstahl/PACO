The function Demo_PACO and Demo_PADDi illustrate two ways to perform speech enhancement via the pitch-adaptive discrete short-time Fourier transform (PADSTFT) proposed in [1] and [4].

The codes rely on [3]. You can find a version of VOICEBOX in the ExternalFunctions folder.

[1] J. Stahl and P. Mowlaee "Exploiting Temporal Correlation in Pitch-Adaptive Speech Enhancement", submitted to Speech Communication, 2018.

[2] J. Stahl and P. Mowlaee "A Simple and Effective Framework for a Priori SNR Estimation", 2018 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP).

[3] VOICEBOX: MATLAB toolbox for speech processing.
    Home page: http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html
