function [x_hat, X_hat]= STFT_SpeechEnhancement(y,varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs Speech Enhancement in STFT domain based on [5].
%
% INPUT:
% y ... noisy speech
% param ... struct containing parameters for the algorithm, if not set, default values are chosen
%                            fs ... sampling frequency
%                   framelength ... framelength (s)
%                           hop ... hop size (s)
%                           win ... window function handle
%                        Estimator ... chosen estimator (e.g. 'LSA')
%                      alpha_DD ... smoothing factor for decision directed approach
%                    FFT_factor ... factor determining fftlength = N*FFT_factor
%                         G_max ... maximum value of gain function
%                         G_min ... floor for spectro-temporal gain function
%     DistributionParameterPath ... if distributions are chosen frequency dependent the correct parameters are found on this path (e.g. './parameters')
%                       zeta_min ... minimum a posteriori SNR
%                       zeta_max ... maximum a posteriori SNR
%                      Floor_xi ... minimum for a priori SNR
%
% OUTPUT:
% x_hat ... enhanced speech
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Detection and Estimation of Stochastic and
%     Deterministic Signal Components for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [3] T. Gerkmann and R. C. Hendriks, “Unbiased MMSE-based noise power
%     estimation with low complexity and low tracking delay,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 20, no. 4, pp. 1383–1393,
%     2012.
% [4] S. Gonzalez and M. Brookes, “PEFAC - a pitch estimation algorithm
%     robust to high levels of noise,” IEEE/ACM Trans. Audio, Speech, and
%     Language Processing, vol. 22, no. 2, pp. 518–530, 2014.
% [5] Y. Ephraim and D. Malah, “Speech enhancement using a minimum-
%     mean square error short-time spectral amplitude estimator,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 32, no. 6, pp. 1109–1121,
%     1984.
% [6] Y. Ephraim and D. Malah, “Speech enhancement using a minimum mean-square error log-
%     spectral amplitude estimator,” IEEE Trans. Audio, Speech, and Language
%     Process., vol. 33, no. 2, pp. 443–445, 1985.
% [7] VOICEBOX: MATLAB toolbox for speech processing.
%     Home page: http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html
% [8] C. Breithaupt, T. Gerkmann, and Rainer Martin,"A Novel A Priori SNR 
%     Estimation Approach based on Selective Cepstro-Temporal Smoothing",
%     2008.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     STFT_SpeechEnhancement.m performs STFT speech enhancement.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (1) Read-in parameters: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p = STFTSpeechEnhancement_InitParams(); % Set parameters according to [1]
[~, p, ~, ~] = parseParams_johannes(p,varargin{:}); % set parameters according to function call

%%%%%%%%%%%%%%%%%%% (2) STFT: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = p.framelength*p.fs; % framelength in samples
[y_framed, t_frames] = FramingAndWindowing(y,p);
fftlength = N*p.FFT_factor;
nbins = fftlength/2+1;
Y = fft(y_framed,fftlength); % half sided stft
Y = Y(1:(nbins),:);

%%%%%%%%%%%%%%%%%%% (3) Noise PSD Estimation [3]: %%%%%%%%%%%%%%%%%%%%%%%%%
hop_noise = p.frameshift;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) PUT YOUR NOISE PSD ESTIMATOR HERE
% 2) for [1] we used [3]      
% 3) should have the variable name D_p and dimensions: (nbins) x #frames                                   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (b) parameters for noise PSD estimation [3]
pH1 = 1/2; % a priori spp
pH0 = 1-pH1;
pfac = pH0./pH1; % a priori probability ratio
xi_H1 = 10^(15/10); % "typical a priori SRN", section IV-D [3]
GH1 = xi_H1./(1+xi_H1); 
p_bar_l_1 = 1; % for avoiding stagnation
alpha_pow = exp(-p.frameshift/0.0717); %0.8, 0.8944 % noise psd estimation
alpha_prob = exp(-p.frameshift/0.152); %0.9, 0.9487 % noise psd estimation

if (strcmp(p.NoisePSDEstimator,'oracle') || strcmp(p.NoisePSDEstimator,'FirstNFrames'))
    [d_framed, t_frames] = FramingAndWindowing(y-p.x,p);
    D = fft(d_framed,fftlength); % half sided stft
    D = D(1:(nbins),:);
    D_p = filter((1-alpha_pow),[1 -alpha_pow],abs(D).^2); % apply first order iir filter
else
    D_p = zeros(size(Y));
end
if (strcmp(p.SpeechCovEstimator,'oracle') || strcmp(p.phase,'oracle'))
    [x_framed, t_frames] = FramingAndWindowing(p.x,p);
    X = fft(x_framed,fftlength); % half sided stft
    X = X(1:(nbins),:);
    sigmax2_l = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (4) Speech Estimation: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init estimation:
G = zeros(size(Y));
xi = zeros(size(Y));
zeta = zeros(size(Y));
xi_unsmoothed_l = 1;
xi_inst_unsmoothed_l = p.xi_min;

% A simple decision directed loop [5]:
hop_ratio = hop_noise/p.frameshift;% ratio of hopsize of noise psd estimator to hopsize of the speech estimator
NrOfFrames = size(y_framed,2);

if isempty(p.alpha_DD)
    p.alpha_DD =  exp(-p.frameshift/0.936); 
end
% Settings for Cepstro Temporal Smoothing:
if strcmp(p.APrioriSNREstimator,'ceps')
    % parameters from table I in [8]
    alpha_pitch = 0.2;
    Delta_q_pitch = 2;
    beta = 0.96;
    tau_H = 8;
    Gamma_thr = 0.2;
    % under question if we should use these parameters:
    %p.xi_min = 10^(-25/10);
    %p.xi_ml_min = 10^(-27/10);
    %p.G_min = 10^(-17/20);%p.G_min;%
    %%%%%%
    f0_min = 70;
    f0_max = 300;
    alpha_bar_const = zeros(N/2+1,1);
    % define intervals for alpha assignment:
    reg1 = 1:3;
    reg2 = 4:20;
    reg3 = 21:(nbins);
    
    alpha_bar_const(reg1) = 0.5;
    alpha_bar_const(reg2) = 0.7;
    alpha_bar_const(reg3) = 0.97;
    alpha_bar_const = [alpha_bar_const; alpha_bar_const((end-1):-1:2)];
    alpha = alpha_bar_const;
    % for log-bias correction:
    kappa_gauss = 0.5772;
    kappa = 0.5*kappa_gauss;
    lambda_ceps_s = zeros(N,NrOfFrames);
    lambda_s_hat = zeros(size(D_p));
    switch p.f0Estimator
        case 'pefac'
            [f0,tf0,pv]= fxpefac(y,p.fs,p.frameshift);
            f0 = interp1(tf0,f0,t_frames,'linear','extrap');
            f0(f0<f0_min) = f0_min;
            f0(f0>f0_max) = f0_max; 
        case 'own'
            f0 = zeros(NrOfFrames,1);
            Nmin = p.framelength*p.fs;
            t_autocorr = (0:(2*Nmin-1))/p.fs;
            T0_max = 1/f0_min;
            T0_min = 1/f0_max;
        case 'oracle'
            if isfield(p,'AnnotationFile')
                ID =fopen(p.AnnotationFile);
                A = textscan(ID,'%f')';
                fclose(ID);
                f0_data = reshape(A{1}',4,length(A{1})/4).';
                tf0 = (f0_data(:,1) + f0_data(:,2))/(2*p.fs);
                f0 = f0_data(:,4);
                f0 = interp1(tf0,f0,t_frames,'linear','extrap');
                f0(f0<f0_min) = f0_min;
                f0(f0>f0_max) = f0_max;
            elseif isfield(p,'CleanSpeech')
                %warning('No Annotation file given, try pefac on the clean speech file ... ')
                [f0,tf0,pv]= fxpefac(p.CleanSpeech,p.fs,p.frameshift);
                f0 = interp1(tf0,f0,t_frames,'linear','extrap');
                f0(f0<f0_min) = f0_min;
                f0(f0>f0_max) = f0_max; 
            else
                %warning('... There was no clean speech file available either, used homegrown estimator... ')
                f0 = zeros(NrOfFrames,1);
                Nmin = p.framelength*p.fs;
                t_autocorr = (0:(2*Nmin-1))/p.fs;
                T0_max = 1/f0_min;
                T0_min = 1/f0_max;
                p.f0Estimator = 'own';
            end
    end
end

if strcmp(p.APrioriSNREstimator,'HRNR')
   tsnra = zeros(nbins,NrOfFrames); 
   newmags = zeros(nbins,NrOfFrames); 
end

for FrameIndex = 1:NrOfFrames
    
    Y_l = Y(:,FrameIndex);
    Y_p_l = Y_l.*conj(Y_l);
	if strcmp(p.NoisePSDEstimator,'mmse')
        if FrameIndex == 1
            D_p_l = mean(Y_p_l);
        end
        if ~mod((FrameIndex-1),hop_ratio)
           pH1_y = 1./(1 + pfac.*(1+xi_H1).*exp(-(Y_p_l ./D_p_l).*GH1)); % eq. (18) [3] 
          
            p_bar_l = alpha_prob.*p_bar_l_1 + (1-alpha_prob).*pH1_y; % eq. (23) [3]
            % avoiding stagnation, Section IV-C:
            pH1_y = min(pH1_y,1-0.01*(p_bar_l>0.99));
            cond_exp_noise_l = (1 - pH1_y).*Y_p_l  + pH1_y.*D_p_l;
            D_p_l =  alpha_pow.*D_p_l + (1-alpha_pow).*cond_exp_noise_l; % eq. (8) [3]
        end
     elseif strcmp(p.NoisePSDEstimator,'FirstNFrames')
        if FrameIndex == 1
            if isempty(p.Nnoise)
                Nnoise = NrOfFrames;
            else
                Nnoise = p.Nnoise;%
            end
            
            D_p_l = diag(D(:,1:Nnoise)*D(:,1:Nnoise)')/Nnoise;
        end
     elseif strcmp(p.NoisePSDEstimator,'Codebook')
        if FrameIndex == 1
           NoiseCodebookPath = '/afs/spsc.tugraz.at/project/ISE/3_w/Ongoing/CorrelationAlongFrequency/NoiseCodebooks/';
           load([NoiseCodebookPath p.NoiseName]); 
           D_p_l = diag(Rd_l);
        end
    else
        D_p_l = D_p(:,FrameIndex);
    end
    
    zeta_l = max(min(Y_p_l./D_p_l,p.zeta_max),p.zeta_min);
    
    
    if ~mod((FrameIndex-1),hop_ratio)
        xi_ml = max(zeta_l-1,p.xi_ml_min);
        if strcmp(p.APrioriSNREstimator,'ceps')
            if strcmp(p.f0Estimator,'own')
                    AutoCorr = abs(fftshift(xcorr(y_framed(:,FrameIndex),y_framed(:,FrameIndex))));
                    [maxval, maxpos] = max(AutoCorr(((t_autocorr>T0_min) & (t_autocorr<T0_max)))); 
                    T0_hat = T0_min+t_autocorr(maxpos); 
                    f0(FrameIndex) = 1/T0_hat;
                    % TODO: voicing decision (i.e. smoothing) based on ratio of maximum to
                    % zero lag?
                    vp(FrameIndex) = maxval/AutoCorr(end);
                if FrameIndex > 1
                   f0(FrameIndex) = f0(FrameIndex-1)*(1-vp(FrameIndex)) + vp(FrameIndex)*f0(FrameIndex);
                   if abs(f0(FrameIndex)-f0(FrameIndex-1))>30
                       f0(FrameIndex) = f0(FrameIndex-1);
                   end
                end
            end
            lambda_ml_s = xi_ml.*D_p(:,FrameIndex); % eq. (3) in [8]
            lambda_ml_ceps_s = rifft(log(lambda_ml_s)); % eq. (4) in [8]
            
            alpha_bar = beta.*alpha + (1-beta).*alpha_bar_const; % Eq. (7) in [8]
            q_pitch = round(p.fs/f0(FrameIndex));
            q_pitch_all_frames(FrameIndex) = q_pitch; 
            Q_pitch = (q_pitch-Delta_q_pitch):(q_pitch+Delta_q_pitch);
            alpha = alpha_bar; % Eq. (6) in [8]
            alpha(Q_pitch) = alpha_pitch; % Eq. (6) in [8]
            alpha = [alpha(1:(nbins)); alpha((fftlength/2):-1:2)];
            alpha_all_frames(:,FrameIndex) = alpha;
            if FrameIndex > 1
            	lambda_ceps_s(:,FrameIndex) = alpha.*lambda_ceps_s(:,FrameIndex-1) + ...
                                              (1-alpha).*lambda_ml_ceps_s; % eq. (5) in [8]
                lambda_s = fft(lambda_ceps_s(:,FrameIndex));
                lambda_s_hat(:,FrameIndex) = real(exp(kappa + lambda_s(1:(N/2+1)))); % eq. (12) in [8]
                xi_l = max(lambda_s_hat(:,FrameIndex)./D_p(:,FrameIndex),p.xi_min); % eq. (13) in [8]
            else
                xi_l = max(p.alpha_DD*xi_unsmoothed_l+(1-p.alpha_DD)*xi_ml,p.xi_min);
            end
        elseif (strcmp(p.APrioriSNREstimator,'DD') || strcmp(p.APrioriSNREstimator,'HRNR') || strcmp(p.APrioriSNREstimator,'Fingscheidt'))
                xi_l = max(p.alpha_DD*xi_unsmoothed_l+(1-p.alpha_DD)*xi_ml,p.xi_min); % Update a priori SNR
        end
    end
    if strcmp(p.SpeechCovEstimator,'oracle')
       % xi_l =  max(p.alpha_DD*xi_unsmoothed_l+(1-p.alpha_DD)*abs(X(:,FrameIndex)).^2./D_p(:,FrameIndex),p.xi_min);
       if FrameIndex > 1
            sigmax2_l = p.alpha_DD*abs(G_l.*Y(:,FrameIndex-1)).^2+(1-p.alpha_DD)*abs(X(:,FrameIndex)).^2  ;
            xi_l =  sigmax2_l./D_p_l;
       else
           xi_l = p.xi_min;
       end
       
    end
    if strcmp(p.APrioriSNREstimator,'Fingscheidt')
        % SPP, Malah1999
        if FrameIndex == 1
           q = 0.5; 
        end
        alpha_q = 0.95;
        eta_min = 0.15; % values between 0.1 and 0.2 according to Malah1999
        alpha0 = 0.50125;
        gamma_th = (1+eta_min)*log(1/(1-alpha0)); % eq. (11) in Malah1999
        gamma = zeta_l;
        I_q = (gamma < gamma_th)*1;
        q = alpha_q*q+(1-alpha_q)*I_q; % eq. (13) in Malah1999
        
        % a priori SNR Fingscheidt2005:
        beta =  1 - q; % eq (20) in Fingscheidt2005
        
        % this is sloppy but on the safe side:
        xi_l_prelim = [xi_l(1) ;xi_l; xi_l(end)];
    	for fbin = 1:nbins
    		xi_l(fbin) = beta(fbin)*(xi_l_prelim(fbin)+xi_l_prelim(fbin+2))/2 + beta(fbin)*xi_l_prelim(fbin+1);
    	end
        % Matrix Multiplication Alternative:
        %Beta = [diag(beta/2) zeros(nbins,2)] + [zeros(nbins,1) diag(beta) zeros(nbins,1)] + [zeros(nbins,2) diag(beta/2)];
        %xi_l = Beta*xi_l_prelim;
    end
    % Gain Computation
	G_l= STFT_GetGain(xi_l,zeta_l,p.Estimator);
    if strcmp(p.APrioriSNREstimator,'HRNR')
        % TSNR SCALART:
        newmag = G_l.*abs(Y(:,FrameIndex));
        tsnr = (newmag.^2) ./ D_p(:,FrameIndex);
        Gtsnr = tsnr ./ (tsnr+1);         %gain of TSNR 
        tsnra(:,FrameIndex)=Gtsnr;    
        Gtsnr = [Gtsnr;Gtsnr((end-1):-1:2)];
        Gtsnr = gaincontrol(Gtsnr,fftlength/2);
        Gtsnr = Gtsnr(1:(nbins));
        %for HRNR use
        newmag = Gtsnr .* abs(Y(:,FrameIndex));
        newmags(:,FrameIndex) = newmag;     %for HRNR use
        % edit Johannes: For Half-Wave Rectifier go back to time domain:
        xhat_tsnr = rifft(newmag.*exp(1i*angle(Y(:,FrameIndex))));
        
        % HRNR:
        newharm = max(xhat_tsnr,0); %rectifier
        ffth = abs(fft(p.win(N).*newharm,fftlength)); ffth = ffth(1:(nbins));
        snrham= ( (tsnra(:,FrameIndex)).*(abs(newmags(:,FrameIndex)).^2) + (1-(tsnra(:,FrameIndex))) .* (ffth.^2) ) ./D_p(:,FrameIndex);
        newgain= (snrham./(snrham+1));
        G_l = newgain;
        G_l = [G_l;G_l((end-1):-1:2)];
        G_l = gaincontrol(G_l,fftlength/2);
        G_l = G_l(1:(nbins));
    end
    
  %  G_l(abs(G_l)<p.G_min) = p.G_min;
    G(:,FrameIndex)  = G_l;
    xi(:,FrameIndex) = xi_l;
    zeta(:,FrameIndex) = zeta_l;
    D_p(:,FrameIndex) = D_p_l;
    xi_unsmoothed_l = zeta_l.*abs(G_l).^2; 
end
% floor gain:
G(abs(G)<p.G_min) = p.G_min;

% apply gain:
X_hat = Y.*G;%abs(Y.*G).*exp(1i*angle(X)); 
%X_hat = p.G_min*Y + Y.*G.*(1-p.G_min);
if strcmp(p.phase,'oracle')
   X_hat = abs(X_hat).*exp(1i*angle(X)); 
elseif strcmp(p.phase,'noisy')
    X_hat = abs(X_hat).*exp(1i*angle(Y));
end
%%%%%%%%%%%%%%%%%%% (5) Signal Synthesis: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_hat_frames = irfft(X_hat,N*p.FFT_factor); % restore full dft

x_hat = STFT_OLA(x_hat_frames,p);
x_hat = [x_hat(1:min(length(y),length(x_hat))); zeros(max(length(y)-length(x_hat),0),1)]; % ensure correct length

end




function        NewGain=gaincontrol(Gain,ConstraintInLength)
%
%Title  : Additional Constraint on the impulse response  
%         to ensure linear convolution property
%
%
%Description : 
%
% 1- The time-duration of noisy speech frame is equal to L1 samples.
%
% 2- This frame is then converted in the frequency domain 
%       by applying a short-time Fourier transform of size NFFT leading
%       to X(wk) k=0,...,NFFT-1 when NFFT is the FFT size.
%
% 3- The estimated noise reduction filter is G(wk) k=0,1,...,NFFT-1 
%       leading to an equivalent impulse response g(n)=IFFT[G(wk)] 
%       of length L2=NFFT
%
% 4- When applying the noise reduction filter G(wk) to the noisy 
%       speech spectrum X(wk), the multiplication S(wk)=G(wk)X(wk) is
%       equivalent to a convolution in the time domain. So the
%       time-duration of the enhanced speech s(n) should be equal to 
%       Ltot=L1+L2-1.
%
% 5- If the length Ltot is greater than the time-duration of the IFFT[S(wk)] 
%       the a time-aliasing effect will appear.
%
% 6- To overcome this phenomenon, the time-duration L2 of the equivalent
%       impulse response g(n) should be chosen such that Ltot = L1 + L2 -1 <= NFFT 
%       => L2 <= NFFT+1-Ll
%
%       here we have NFFT=2*Ll so we should have L2 <= Ll+1. I have made
%       the following choice : the time-duration of g(n) is limited to
%       L2=NFFT/2=L1 (see lines 88 and 192)
%
%Author : SCALART Pascal
%
%October  2008
%


meanGain=mean(Gain.^2);
NFFT=length(Gain);

L2=ConstraintInLength;

win=hamming(L2);

% Frequency -> Time
% computation of the non-constrained impulse response
ImpulseR=real(ifft(Gain));

% application of the constraint in the time domain
ImpulseR2=[ImpulseR(1:L2/2).*win(1+L2/2:L2);zeros(NFFT-L2,1);ImpulseR(NFFT-L2/2+1:NFFT).*win(1:L2/2)];

% Time -> Frequency
NewGain=abs(fft(ImpulseR2,NFFT));

meanNewGain=mean(NewGain.^2);

NewGain=NewGain*sqrt(meanGain/meanNewGain); % normalisation to keep the same energy (if white r.v.)

end

function param = STFTSpeechEnhancement_InitParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializaes parameters for PACO. If PACO is called with different
% parameter settings, the default values are overwritten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
param = struct( 'x', [], ... % clean input signal
                'Estimator', 'WienerFilter', ... % choose which gain to apply for innovation process estimation
                'framelength', 0.032, ...   % time domain frame length
                'frameshift', 0.008, ...    % sliding window shift
                'fs',16e3,... % sampling frequency
                'G_min', 0.1, ... % maximum noise suppression
                'G_max', 1, ...
                'alpha_DD', [],... % smoothing constant for speech variance
                'window', @(N) sqrt(hamming(N,'periodic')), ... % chosen window function
                'APrioriSNREstimator', 'DD', ... % chose a priori SNR estimator
                'xi_ml_min', 0, ... % defines minimum ratio of speech variance ml estimate and noise variance 
                'xi_min', 0.001, ... %10^(-1.5), ... % defines minimum ratio of speech and noise variance 
                'zeta_min', 0.001, ... % minimum a posteriori SNR, only important if gain functions other than the wiener filter are applied
                'zeta_max', 1000, ... % maximum a posteriori SNR, only important if gain functions other than the wiener filter are applied
                'FFT_factor', 1, ...
                'NoisePSDEstimator', 'mmse', ...
                'NoiseName', '', ...
                'Nnoise', [], ...
                'SpeechCovEstimator' ,'DD', ...
                'phase', 'estimated', ...
                'f0_min', 90,.... % minmum fundamental frequency
                'f0_max', 350,... % maximum fundamental frequency
                'f0', [], ... % default fundmanetal frequency input
                'f0Oracle', 1, ...
                'ARData', 'Xtilde', ...
                'ARCoefficientsType', 'DecomposedComplexCoeffs', ... % type of AR coefficients considered, 'DecomposedComplexCoeffs'/ 'ComplexCoeffs'
                'NAR', 1, ... % AR model order
                'bufferlength', 0.064, ... % buffer size that is taken into account for AR parameter estimation
                'Codebook', 1, ...
                'plotting', 0 ...
                ); 
end
