function G_l= STFT_GetGain(xi_l,zeta_l,Method)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes Gain for various estimators.
%
% INPUT:
% xi_l ... a priori SNR at frame l
% zeta_l ... a posteriori SNR at frame l
% Method ... chosen estimator
%
% OUTPUT:
% G_l ... gain for frame l
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] P.J. Wolfe, S.J. Godsill "Efficient alternatives to the Ephraim and
%     Malah suppression rule for audio signal enhancement" (2003), pp. 1043–1051
% [2] Y. Ephraim and D. Malah, “Speech enhancement using a minimum-
%     mean square error short-time spectral amplitude estimator,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 32, no. 6, pp. 1109–1121,
%     1984.
% [3] Y. Ephraim and D. Malah, “Speech enhancement using a minimum mean-square error log-
%     spectral amplitude estimator,” IEEE Trans. Audio, Speech, and Language
%     Process., vol. 33, no. 2, pp. 443–445, 1985.
% [4]  T. Lotter, P. Vary "Speech enhancement by MAP spectral amplitude
%      estimation using a super-gaussian speech model" EURASIP J. Adv.Signal Process., 2005 (7) (2005), pp. 1110–1126
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     STFT_GetGain.m computes Gain for various estimators.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% default compute gain as proposed in [2]
vk = xi_l.*zeta_l./(1+xi_l); % eq. 8 [1]
G_l_EM = (0.277 + vk)./zeta_l; % accurate to 0.02 dB for v>1
if any(vk<1)
   G_l_EM(vk<1) = gamma(1.5).*sqrt(vk(vk<1)).*((1+vk(vk<1))...
                  .*besseli(0,vk((vk<1))/2) + vk(vk<1).*besseli(1,vk(vk<1)/2))./(zeta_l(vk<1).*exp(vk(vk<1)/2)); % eq. 7 [1] 
end

switch Method
    case 'WienerFilter'
        G_l = xi_l./(1+xi_l);
	case 'MMSESTSA' % [1]
        G_l = G_l_EM; % [2]
    case 'LSA'
        G_l = xi_l./(1+xi_l).*exp(0.5*expint(xi_l./(1+xi_l).*zeta_l));
        G_l(isnan(G_l)) = 0;
	case 'JMAPLV' % [3]
        nu = 0.2;
        mu = sqrt((nu+1)*(nu+2));
        u = 1/2 - mu./(4*sqrt(zeta_l.*xi_l));
        G_l = u + sqrt(u.^2 + nu./(2*zeta_l));
	case 'JMAPGW' % [4]
    	G_l = (xi_l + sqrt(xi_l.^2 +2*(1+xi_l).*xi_l./zeta_l))./(2*(1+xi_l));
end

end
