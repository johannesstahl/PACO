function [x_framed, t_frames] = FramingAndWindowing(x,param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% splits signal into (overlapping) frames and multiplies them with the selected window
% function (fixed sliding window).
% 
% INPUT:
% x ... input signal
% param ... contains parameters:
%           win ... handle for window function
%           framelength ... frame lenght in seconds
%           fs ... sampling frequency
%           frameshift/hop ... frameshiftsize in seconds
%
% OUTPUT:
% x_framed ... framed and windowed signal
% t_frames ... timestamps of individual frames in seconds 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 08.09.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     FramingAndWindowing.m splits signal into (overlapping) frames and multiplies them with the selected window function (fixed sliding window).
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the frameshifte that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if any(strcmp(fieldnames(param),'hop'))
   param.frameshift = param.hop; % for backwards compatibility with older codes 
end
if isfield(param,'window')
    param.win = param.window;
end
N = round(param.framelength*param.fs);
frameshift = round(param.frameshift*param.fs);
window = param.win(N);
window= window/(sqrt(sum(window(1:frameshift:N).^2))); % normalization
if any(isnan(window))
   window = param.win(N); 
end
x_framed = buffer(x,N,N-frameshift,'nodelay'); % framing the signal
NrOfFrames = size(x_framed,2); 
x_framed = x_framed.*window(:,ones(1,NrOfFrames)); % windowing the signal
t_frames = ((N/2+1):frameshift:(length(x)-N/2 + 1))/param.fs; % frame-center time-stamps
if t_frames < size(x_framed,2)
   t_frames = [t_frames t_frames(end)+frameshift/param.fs];
end
end