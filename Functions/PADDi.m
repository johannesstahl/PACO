function x_hat = PADDi(y,varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of the pitch-adaptive decision-directed (PADDi) + Wiener
% Filter speech enhcancement method proposed in [1]
%
% INPUT:
% y ... noisy speech
% varargin: parameters can be set by e.g.,: PADDi(y,'fs',16e3);
%           for a list of parameters see PADDi_InitParams.m
% OUTPUT:
% x_hat ... enhanced speech
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 02.02.2018
% matlab version: 2017b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Exploiting Temporal Correlation for 
%     Pitch-Adaptive Complex-Valued Speech Enhancement", submitted to 
%     IEEE/ACM Trans. Audio, Speech, and Language Processing, 2018.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [3] T. Gerkmann and R. C. Hendriks, “Unbiased MMSE-based noise power
%     estimation with low complexity and low tracking delay,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 20, no. 4, pp. 1383–1393,
%     2012.
% [4] J. Stahl and P. Mowlaee "A Simple and Effective Framework for A priori SNR estimation", 
%     appear in Proc. ICASSP 2018.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PADDi.m applies PADSTFT and performs Wiener filtering.
%     Copyright (C) 2018 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%% (A) Read-in parameters: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p = PADDi_InitParams(); % Set parameters
[~, p, ~, ~] = parseParams_johannes(p,varargin{:}); % set parameters according to function call


%%%%%%%%%%%%%%%%%%% (B) apply sliding window: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[y_framed, t_frames] = FramingAndWindowing(y,p); 
NrOfFrames = length(t_frames);

%%%%%%%%%%%%%%%%%%% (C) f0-estimation: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isempty(p.f0)
    if ~strcmp(p.f0Estimator,'own')
        [f0,tf0,~]= fxpefac(y,p.fs,p.frameshift);
    end
else
   f0 = p.f0(:,2);
   tf0 = p.f0(:,3);
end
f0 = interp1(tf0,f0,t_frames,'linear','extrap');
f0(f0<p.f0_min) = p.f0_min;
f0(f0>p.f0_max) = p.f0_max;

% (a) Setup PADSTFT:
Nmin = p.framelength*p.fs;
K0 = ceil(Nmin/p.fs*p.f0_max);
Nmax = ceil(K0*p.fs/p.f0_min); Nmax = Nmax + mod(Nmax,2);
MaxNrOfFrequencyBins = floor(Nmax/2+1);
fk_FR = (0:(Nmin/2))*p.fs/Nmin; % fixed resolution frequency bins, needed for interpolation of noise PSD


%%%%%%%%%%%%%%%%%%% (D) Speech Estimation: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init estimation:

xi_unsmoothed_l = 1;
%alpha_DD(f_FR>4e3) = p.alpha_DD_uv;

% (b) parameters for noise PSD estimation [3]
pH1 = 1/2; % a priori spp
pH0 = 1-pH1;
pfac = pH0./pH1; % a priori probability ratio
xi_H1 = 10^(15/10); % "typical a priori SRN", section IV-D [3]
GH1 = xi_H1./(1+xi_H1); 
p_bar_l_1 = 1; % for avoiding stagnation

% (c) smoothing constants:
alpha_DD =  exp(-p.frameshift/0.936); %  speech variance estimation
alpha_pow = exp(-p.frameshift/0.0717); %0.8, 0.8944 % noise psd estimation
alpha_prob = exp(-p.frameshift/0.152); %0.9, 0.9487 % noise psd estimation

NrOfFrames = min(NrOfFrames,length(f0));
for FrameIndex = 1:NrOfFrames
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%% (0) Estimate f0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     if strcmp(p.f0Estimator,'own') % as proposed in [1]
        if FrameIndex == 1
        	t_autocorr = (0:(2*Nmin-1))/fs;
        end
        AutoCorr = abs(fftshift(xcorr(y_framed(:,FrameIndex),y_framed(:,FrameIndex))));
        [~, maxpos] = max(AutoCorr(((t_autocorr>T0_min) & (t_autocorr<T0_max)))); 
        T0_hat = T0_min+t_autocorr(maxpos); 
        f0(FrameIndex) = 1/T0_hat;
        if FrameIndex > 1
           if abs(f0(FrameIndex)-f0(FrameIndex-1))>30
               f0(FrameIndex) = f0(FrameIndex-1);
           end
        end
     end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%% (1) Frame Setup %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    N0 = max(round(K0*p.fs/f0(FrameIndex)),Nmin); % obtain frame length from f0 estimate
    N0 = N0 + mod(N0,2); % ensure that DFT length is even
    fk = (0:(N0/2))/N0*p.fs; % PADFT frequencies
    fftbins = 1:(N0/2+1); % fft bins up to nyquist bin
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%% (3) Compute Y(k,l) %%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Y = fft(y_framed(:,FrameIndex),N0);
    Y_l = zeros(MaxNrOfFrequencyBins,1); % initialize observation at frame l
    Y_l(fftbins) = Y(fftbins); 
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%% (4) Estimate Noise PSD %%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % (a) Noise PSD estimation [3] 
    D_p_l = zeros(MaxNrOfFrequencyBins,1); % allocate noise psd matrix
    if p.InterpolateNoisePSD
        Y_FR = fft(y_framed(:,FrameIndex)); 
        Y_FR = Y_FR(1:(Nmin/2+1),:);
        Y_pow_l = abs(Y_FR).^2;
        if FrameIndex == 1
            D_p_FR = mean(Y_pow_l).*ones(size(Y_pow_l));
        end
        pH1_y = 1./(1 + pfac.*(1+xi_H1).*exp(-(Y_pow_l ./D_p_FR).*GH1)); % eq. (18) [3]
        p_bar_l = alpha_prob.*p_bar_l_1 + (1-alpha_prob).*pH1_y; % eq. (23) [3]
        % avoiding stagnation, Section IV-C:
        pH1_y = min(pH1_y,1-0.01*(p_bar_l>0.99));
        cond_exp_noise_l = (1 - pH1_y).*Y_pow_l  + pH1_y.*D_p_FR;
        D_p_FR =  alpha_pow.*D_p_FR + (1-alpha_pow).*cond_exp_noise_l; % eq. (8) [3]
        % linearly interpolate to PADSTFT:
        D_p_l(fftbins) = max(interp1(fk_FR,D_p_FR, fk,'linear','extrap'),0);
    else
        % assume that first frame is noise only:
        if FrameIndex == 1
            InitBins = ones(MaxNrOfFrequencyBins,1);
            var_noise_l_1 = zeros(MaxNrOfFrequencyBins,1);
        end
        Y_pow_l = abs(Y_l).^2;    
        if sum(InitBins)>0
            var_noise_l_1 = var_noise_l_1 + InitBins.*mean(Y_pow_l(InitBins==1));
            InitBins(Y_pow_l>0) = 0;
        end
        pH1_y = 1./(1 + pfac.*(1+xi_H1).*exp(-(Y_pow_l ./var_noise_l_1).*GH1)); % eq. (18) [3]
        p_bar_l = alpha_prob.*p_bar_l_1 + (1-alpha_prob).*pH1_y; % eq. (23) [3]
        % avoiding stagnation, Section IV-C:
        pH1_y = min(pH1_y,1-0.01*(p_bar_l>0.99));
        cond_exp_noise_l = (1 - pH1_y).*Y_pow_l  + pH1_y.*var_noise_l_1;
        D_p_l =  alpha_pow.*var_noise_l_1 + (1-alpha_pow).*cond_exp_noise_l; % eq. (8) [3]
        % this is PADSTFT specific:
        % if DFT length has changed, store past values for higher frequency 
        % bins in order to get rid of fluctuations:
        D_p_l(Y_pow_l == 0) = var_noise_l_1(Y_pow_l == 0); 
        var_noise_l_1 = D_p_l;
    end
	D_p_l(Y_l == 0) = 0;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%% (5) Estimate Speech %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % PADDi [1]:
    zeta_l = max(min(abs(Y_l).^2./D_p_l ,p.zeta_max),p.zeta_min);
    xi_l = max(alpha_DD.*xi_unsmoothed_l+(1-alpha_DD).*max(zeta_l-1,p.xi_ml_min),p.xi_min); % Update a priori SNR
    zeta_l(Y_l==0) = 0;
    xi_l(Y_l==0) = p.xi_min;
	G_l= STFT_GetGain(xi_l,zeta_l,p.Estimator);
    G_l(Y_l==0) = 0;
    xi_unsmoothed_l = zeta_l.*abs(G_l).^2; 
    % floor gain:
    G_l(abs(G_l)<p.G_min) = p.G_min;
    
    X_hat_l = Y_l.*G_l; 
    X_hat_l = X_hat_l(fftbins);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%% (6) Apply inverse DFT and windowing %%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    x_hat_l = 2*real(ifft(X_hat_l,N0)); % obtain time domain frame
    x_hat_frames(1:min(p.framelength*p.fs,N0),FrameIndex) = x_hat_l(1:min(p.framelength*p.fs,N0)); 
end

%%%%%%%%%%%%%%%%%%% (E) Signal Synthesis: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_hat = STFT_OLA(x_hat_frames,p);
x_hat = [x_hat(1:min(length(y),length(x_hat))); zeros(max(length(y)-length(x_hat),0),1)]; % ensure correct length

end

function param = PADDi_InitParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializaes parameters for PADDi, if PADDi is called with different
% parameter settings, the default values are overwritten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
param = struct( 'framelength', 0.032, ...% time domain frame length
                'frameshift', 0.008, ... % sliding window shift
                'Estimator', 'WienerFilter', ...% choose which gain to apply for speech estimation
                'G_min', 0.1, ...% maximum noise suppression
                'alpha_DD', 0.98,... % smoothing constant for speech variance
                'f0_min', 90,... % minmum fundamental frequency
                'f0_max', 350,... % maximum fundamental frequency
                'f0', [], ...  % default fundmanetal frequency input
                'fs',16e3,... % sampling frequency
                'window', @(N) sqrt(hamming(N,'periodic')), ... % chosen window function
                'xi_ml_min', 0, ... % defines minimum ratio of speech variance ml estimate and noise variance 
                'xi_min', 0, ... % defines minimum ratio of speech and noise variance 
                'zeta_min', 0.001, ... % minimum a posteriori SNR, only important if gain functions other than the wiener filter are applied
                'zeta_max', 1000, ... % maximum a posteriori SNR, only important if gain functions other than the wiener filter are applied
                'G_max', 1, ...
                'f0Estimator', 'pefac', ... % chosen f0 estimator if no input f0 estimates are given 'pefac'/'own'
                'InterpolateNoisePSD', 1 ... % flag that indicates wether the noise PSD is estimated in DSTFT (=1) or PADSTFT (=0).
                ); 
end