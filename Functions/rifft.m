function x = rifft(X,nfft)
% X has frequency channels as first dimension
if nargin < 2
   nfft =  2*(length(X)-1); % automatically assume even fft length
end
X(1) = X(1)/2;
if mod(nfft,2) == 0
   X(end) = X(end)/2; 
end
x = 2*real(ifft(X,nfft));

end