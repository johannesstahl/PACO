function x_hat = PACO(y,varargin) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of the pitch-adaptive complex-valued Kalman filter (PACO)
% proposed in [1]
%
% INPUT:
% y ... noisy speech
% varargin: parameters can be set by e.g.,: PACO(y,'fs',16e3);
%           for a list of parameters see functions PACO_InitParams()
% OUTPUT:
% x_hat ... enhanced speech
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 02.02.2018
% matlab version: 2017b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Exploiting Temporal Correlation in 
%     Pitch-Adaptive Speech Enhancement", submitted to 
%     Speech Communication, 2018.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [3] T. Gerkmann and R. C. Hendriks, “Unbiased MMSE-based noise power
%     estimation with low complexity and low tracking delay,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 20, no. 4, pparam. 1383–1393,
%     2012.
% [4] S. Gonzalez and M. Brookes, “PEFAC - a pitch estimation algorithm
%     robust to high levels of noise,” IEEE/ACM Trans. Audio, Speech, and
%     Language Processing, vol. 22, no. 2, pparam. 518–530, 2014.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PACO.m applies complex Kalman filter in pitch adaptive STFT.
%     Copyright (C) 2018 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%% (A) Read-in parameters: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
param = PACO_InitParams(); % Set parameters according to [1]
[~, param, ~, ~] = parseParams_johannes(param,varargin{:}); % set parameters according to function call
fs = param.fs;
%%%%%%%%%%%%%%%%%%% (2) STFT with adaptive resolution: %%%%%%%%%%%%%%%%%%%%
[y_framed, t_frames] = FramingAndWindowing(y,param);
NrOfFrames = size(y_framed,2);

f0_min = 90;
f0_max = 350;
T0_max = 1/f0_min;
T0_min = 1/f0_max;
if strcmp(param.f0Estimator,'pefac')
    [f0,tf0,pv]= fxpefac(y,param.fs,param.frameshift);
    f0 = interp1(tf0,f0,t_frames,'linear','extrap');
    f0(f0<f0_min) = f0_min;
    f0(f0>f0_max) = f0_max;
    
elseif strcmp(param.f0Estimator,'own')
    
elseif strcmp(param.f0Estimator,'oracle')
    if isfield(param,'AnnotationFile')
                ID =fopen(param.AnnotationFile);
                A = textscan(ID,'%f')';
                fclose(ID);
                f0_data = reshape(A{1}',4,length(A{1})/4).';
                tf0 = (f0_data(:,1) + f0_data(:,2))/(2*param.fs);
                f0 = f0_data(:,4);
                f0 = interp1(tf0,f0,t_frames,'linear','extrap');
                f0(f0<f0_min) = f0_min;
                f0(f0>f0_max) = f0_max;
            elseif isfield(param,'CleanSpeech')
                %warning('No Annotation file given, try pefac on the clean speech file ... ')
                [f0,tf0,pv]= fxpefac(param.CleanSpeech,param.fs,param.frameshift);
                f0 = interp1(tf0,f0,t_frames,'linear','extrap');
                f0(f0<f0_min) = f0_min;
                f0(f0>f0_max) = f0_max; 
            else
                %warning('... There was no clean speech file available either, used homegrown estimator... ')
                f0 = zeros(NrOfFrames,1);
                Nmin = param.framelength*param.fs;
                t_autocorr = (0:(2*Nmin-1))/param.fs;
                T0_max = 1/f0_min;
                T0_min = 1/f0_max;
                param.f0Estimator = 'own';
    end
end

Nmin = param.framelength*fs;
K0 = ceil(Nmin/fs*f0_max);
Nmax = ceil(K0*fs/f0_min);
Nmax = Nmax + mod(Nmax,2);
fk = cell(NrOfFrames,1);
N0 = zeros(size(fk));
t_autocorr = (0:(2*Nmin-1))/fs;

%%%%%%%%%%%%%%%%%%% (3) Noise PSD Estimation [3]: %%%%%%%%%%%%%%%%%%%%%%%%%
hop_noise = param.frameshift;

Y_FR = fft(y_framed);
Y_FR = Y_FR(1:(Nmin/2+1),:);

D_p_FR =estnoiseg(abs((Y_FR.').*(Y_FR')),hop_noise).';   

f_FR = (0:(Nmin/2))*param.fs/Nmin;
G_FR = zeros(size(Y_FR));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) PUT YOUR NOISE PSD ESTIMATOR HERE
% 2) for [1] we used [3]      
% 3) should have the variable name D_p and dimensions: (fftlength/2+1) x #frames                                   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (6) Speech Estimation: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init estimation:

xi_unsmoothed_l = 1;


% A simple decision directed loop [5]:
hop_ratio = hop_noise/param.frameshift;% ratio of hopsize of noise psd estimator to hopsize of the speech estimator

% setup PS STDFT:
fftlength = Nmax;

Y = zeros(Nmax,NrOfFrames);
G = zeros(Nmax/2+1,NrOfFrames);
xi = zeros(Nmax/2+1,NrOfFrames);
zeta = zeros(Nmax/2+1,NrOfFrames);
X_hat = zeros(Nmax/2+1,NrOfFrames);
D_p = zeros(Nmax/2+1,NrOfFrames);
x_hat_frames=zeros(size(y_framed));
HarmonicBins = ((K0+1):K0:(fftlength/2+1)).'; 
HarmonicBins = sort([HarmonicBins; HarmonicBins+1; HarmonicBins-1]);
HarmonicBins(HarmonicBins>(Nmax/2+1)) = [];

% s
DD_weight = zeros(Nmax/2+1,1);
DD_weight(HarmonicBins) = 1;
alpha_DD = param.alpha_DD;%param.alpha_DD_v*DD_weight;
alpha_DD(alpha_DD ==0) = param.alpha_DD; %param.alpha_DD_uv;
%alpha_DD(f_FR>4e3) = param.alpha_DD_uv;

%%%%%%%%%%%%%% inits for PACO:
PhaseProg = zeros(NrOfFrames+1,1);
MaxNrOfFrequencyBins = floor(Nmax/2+1);
fk_FR = (0:(Nmin/2))*param.fs/Nmin; % fixed resolution frequency bins, needed for interpolation of noise PSD
HarmonicIndices = (1+K0):K0:(MaxNrOfFrequencyBins); 
HarmonicToDFT = zeros(MaxNrOfFrequencyBins,1); % assigns dominant harmonics to corresponding DFT bins  
HarmonicToDFT(round((1+K0)/2):(MaxNrOfFrequencyBins))=interp1(HarmonicIndices,1:length(HarmonicIndices), ...
                                        round((1+K0)/2):(MaxNrOfFrequencyBins),'nearest','extrap');
a_bla = zeros(NrOfFrames,MaxNrOfFrequencyBins,param.NAR);
X_up = zeros(MaxNrOfFrequencyBins,NrOfFrames); % initialize the update matrix
X_up_tilde = zeros(MaxNrOfFrequencyBins,NrOfFrames); % initialize the update matrix 
Y_tilde = zeros(floor(MaxNrOfFrequencyBins),NrOfFrames); % % initialize observation matrix 
alpha_B = param.alpha_B;
B_l = repmat(zeros(param.NAR+1),1,1,MaxNrOfFrequencyBins); B_l = permute(B_l,[3 1 2]);
rAA = ones(param.NAR,1);
RAA = ones(param.NAR,param.NAR);
P_lMinus1 = zeros(MaxNrOfFrequencyBins,1); % initialize error covariance
for FrameIndex = 1:NrOfFrames
    
    N0(FrameIndex) = max(round(K0*param.fs/f0(FrameIndex)),Nmin);
    N0(FrameIndex) = N0(FrameIndex) + mod(N0(FrameIndex),2);
    fk{FrameIndex} = (0:(N0(FrameIndex)/2))/N0(FrameIndex)*param.fs;
    fftbins = 1:(N0(FrameIndex)/2+1); % fft bins up to nyquist bin
    
    D_p(fftbins,FrameIndex) = max(interp1(f_FR,D_p_FR(:,FrameIndex),fk{FrameIndex},'linear','extrap'),0);
    Y(1:N0(FrameIndex),FrameIndex) = fft(y_framed(:,FrameIndex),N0(FrameIndex));
    Y_l = zeros(Nmax/2+1,1);
    Y_l(fftbins) = Y(fftbins,FrameIndex);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%% (2) Estimate Phase Progression %%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % PhaseWrapper ... Window Impact
    [PhaseWrapper(:,FrameIndex),PhaseProg] = Get_PhaseWrapper(PhaseProg,...
                                                f0, ...
                                                N0(FrameIndex), ...
                                                fk{FrameIndex}, ...
                                                Nmin, ...
                                                HarmonicToDFT, ...
                                                MaxNrOfFrequencyBins, ...
                                                FrameIndex, ...
                                                param.fs, ...
                                                param ...
                                                );
    
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %%%%%%%%%%%%%%%%%%%%%%%%% (4) Estimate a(k,l) %%%%%%%%%%%%%%%%%%%%%%%%%
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     % (a) Obtain initial Phase by decomposing phase, eq. (20) [1]:
     Y_tilde_l = Y_l.*conj(PhaseWrapper(:,FrameIndex));
     Y_tilde(:,FrameIndex) = Y_tilde_l; 
    
% (c) Estimate AR parameters
        if FrameIndex > param.NAR +1

            for vv = 1:(param.NAR+1)
                for uu = 1:(param.NAR+1) 
                    if uu == 1
                        xuu = Y_tilde(:,FrameIndex);
                    else
                        xuu = X_up_tilde(:,FrameIndex-uu+1);
                    end
                    if vv==1
                        xvv = Y_tilde(:,FrameIndex);
                    else
                        xvv = X_up_tilde(:,FrameIndex-vv+1);
                    end                  
                    B_l_inst = exp(1i*angle(xuu./xvv));
                    B_l_inst(isnan(B_l_inst)) = 0;
                    B_l(:,uu,vv) = (alpha_B.*squeeze(B_l(:,uu,vv)) + (1-alpha_B).*B_l_inst);  
                end
            end
            
        
            Rxx = abs(squeeze(B_l(:,2:end,2:end))).*RAA; 
            rxx = abs(squeeze(B_l(:,2:end,1))).*rAA.';
            a_bla(FrameIndex,:) = rxx./squeeze(Rxx);
            a_bla(FrameIndex,abs(Y_l)==0) = 0;
       
            a_bla(isnan(a_bla)) = 0;
            a_bla(isinf(a_bla)) = 0;
            a_bla(a_bla>1) = 0;
            a_bla = min(a_bla,0.99);
            
        end
        
        
        %a_bla(FrameIndex,newcomps | oldcomps,:) = 0;
        a_l = -squeeze(a_bla(FrameIndex,:)); % the minus is under question
       % a_l(abs(a_l)<0.7) = 0;
        
        


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%% (5) Propagate State Estimates %%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	xvec = Get_xvec(X_up, X_up_tilde, FrameIndex, param.NAR,param.ARCoefficientsType);
    X_prop_l = PropagateStateEstimate(a_l,xvec).*PhaseWrapper(:,FrameIndex);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%% (6) Compute variances %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % (b) Innovation process variance, Section IV-B, [1]
    
    X_in_l = Y_l - X_prop_l; 
    
    zeta_l = max(min(abs(X_in_l(fftbins)).^2./D_p(fftbins,FrameIndex) ,param.zeta_max),param.zeta_min);
    zeta(fftbins,FrameIndex) = zeta_l;
    zeta_l =  zeta(:,FrameIndex);
    
    if ~mod((FrameIndex-1),hop_ratio)
       alpha_DD_mix = alpha_DD;
       xi_l = max(alpha_DD_mix.*xi_unsmoothed_l+(1-alpha_DD_mix).*max(zeta_l-1,param.xi_ml_min),param.xi_min); % Update a priori SNR
 	end
    
    sigmax2_l = xi_l.*D_p(:,FrameIndex); % eq. (32) [1]
        
        
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%% (7) Propagate error covariance %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    P_l =   min(diag(a_l'*a_l),1).*P_lMinus1 + sigmax2_l; % eq. (31), [1]
    P_l(abs(Y_l)==0) = 0;
	xi_l = P_l./D_p(:,FrameIndex);
    
    % ?:
    xi_l(abs(Y_l)==0) = 0;    
    %xi_l(Y_l==0) = param.xi_min;
	
    G_l= STFT_GetGain(xi_l,zeta_l,param.Estimator);
    
    xi(:,FrameIndex) = xi_l;
    G_l(abs(Y_l)==0) = 0;
    xi_unsmoothed_l = zeta_l.*abs(G_l).^2; 
    X_in_l = X_in_l.*G_l; % eq. (29), [1]
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%% (9) Update state estimate and error covariance %%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    X_up_l = X_prop_l + X_in_l; % eq. (30), [1]
    X_up_l(abs(Y_l)==0)=0;
    X_up_l(isnan(X_up_l)) = 0;
    
    P_lMinus1 = (1-G_l).*P_l; P_lMinus1(abs(Y_l)==0)=0; % eq. (30), [1]
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%% (10) Floor the applied gain %%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % bound the maximum noise suppression and select reconstruction phase
    switch param.ReconstPhase
        case 'kalman'
            Phase = angle(X_up_l); %exp(1i*angle(X_up_l));%% eq. (35) [1]
        case 'mix'
            Phase = angle(param.G_min*Y_l + (1-param.G_min)*X_up_l); %exp(1i*angle(X_up_l));%% eq. (35) [1]
        case 'noisy'
            Phase = angle(Y_l); %exp(1i*angle(X_up_l));%% eq. (35) [1]
    end
    X_hat_l = max(abs(X_up_l),abs(Y_l).*param.G_min).*exp(1i*Phase);
    X_hat_l(abs(Y_l)==0) = 0;
 
    
    
    % write into matrices:
    X_up(:,FrameIndex) = X_up_l;%X_hat_l;
    X_up_tilde(:,FrameIndex) =  X_up_l.*conj(PhaseWrapper(:,FrameIndex)); % X_hat_l.*conj(PhaseWrapper(:,FrameIndex)); %
    X_prop(:,FrameIndex) = X_prop_l.*conj(PhaseWrapper(:,FrameIndex)); %X_hat_l;
    a(:,FrameIndex) = squeeze(a_l);
    X_hat(:,FrameIndex) = X_hat_l;
    X_hat_l = X_hat_l(fftbins);
    x_hat_l = rifft(X_hat_l,N0(FrameIndex));
    %x_hat_l = ifft([X_hat_l;conj(X_hat_l((end-1):-1:2,:))],N0(FrameIndex)); % restore full dft
    %x_hat_l = irfft(X_hat_l)
    x_hat_frames(1:min(param.framelength*param.fs,N0(FrameIndex)),FrameIndex) = x_hat_l(1:min(param.framelength*param.fs,N0(FrameIndex))); 
   
end
%%%%%%%%%%%%%%%%%%% (7) Signal Synthesis: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%x_hat_frames = real(irfft(G_FR.*Y_FR));
x_hat = STFT_OLA(x_hat_frames,param);
x_hat = [x_hat(1:min(length(y),length(x_hat))); zeros(max(length(y)-length(x_hat),0),1)]; % ensure correct length

%PH1PH0 = exp(zeta.*xi./(xi+1))./(xi+1);
Y = Y(1:(Nmax/2+1),:);

end


function [PhaseWrapper,PhaseProg] = Get_PhaseWrapper(PhaseProg,f0,N0,fk,Nmin,HarmonicToDFT,MaxNrOfFrequencyBins,FrameIndex,fs,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PhaseWrapper is needed to decompose the phase
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    LinearWindowPhase = zeros(MaxNrOfFrequencyBins,1);
    omega = 2*pi*fk.'/fs; % normalized angular frequency of PADFT 
    LinearWindowPhase(1:floor(N0/2+1)) = exp(-1i*omega*Nmin/2); % compute linear window phase
    
	%  Integrate fundamental frequency along time for phase progression, eq. (7) [1]:
	f0_int = (FrameIndex == 1)*2*f0(FrameIndex) + (FrameIndex ~= 1)*(f0(FrameIndex) + f0(FrameIndex-1 + (FrameIndex == 1))); 
    PhaseProg(FrameIndex+1) = PhaseProg(FrameIndex) +  p.frameshift*pi*f0_int;
    PhaseProgression_l = exp(1i*HarmonicToDFT(1:floor(MaxNrOfFrequencyBins))*PhaseProg(FrameIndex+1)); 
    PhaseWrapper = PhaseProgression_l.*LinearWindowPhase;

end

function xvec = Get_xvec(X_up, X_up_tilde, FrameIndex, FilterOrder,ARCoefficientsType)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update state estimate vector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    prev_samples = max(FrameIndex-1,1):-1:(FrameIndex-FilterOrder);
    switch ARCoefficientsType
        case 'ComplexCoeffs'
            xvec = X_up(:,prev_samples(prev_samples > 0)).';
        case 'DecomposedComplexCoeffs'
            xvec = X_up_tilde(:,prev_samples(prev_samples > 0)).';
    end
    
    if size(xvec,1)<FilterOrder
       xvec = [xvec; zeros(FilterOrder - size(xvec,1),size(xvec,2))]; 
    end
    
end




function X_prop_l = PropagateStateEstimate(a_l,x_vec)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% propagate current state estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if size(a_l,1) == 1
        X_prop_l = -(a_l.*x_vec).'; % propagate last estimate
	else
    	X_prop_l = -diag(a_l.'*x_vec); % propagate last estimate
	end
end


function param = PACO_InitParams()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializaes parameters for PACO. If PACO is called with different
% parameter settings, the default values are overwritten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
param = struct( 'framelength', 0.032, ...   % time domain frame length
                'frameshift', 0.008, ...    % sliding window shift
                'ARCoefficientsType', 'DecomposedComplexCoeffs', ... % type of AR coefficients considered, 'DecomposedComplexCoeffs'/ 'ComplexCoeffs'
                'Estimator', 'WienerFilter', ... % choose which gain to apply for innovation process estimation
                'ReconstPhase','mix',...%'mix', ...
                'NAR', 1, ... % AR model order
                'NB', 8, ... % buffer size that is taken into account for AR parameter estimation
                'G_min', 0.1, ... % maximum noise suppression
                'alpha_DD', 0.98,... % smoothing constant for speech variance
                'f0_min', 90,.... % minmum fundamental frequency
                'f0_max', 350,... % maximum fundamental frequency
                'f0', [], ... % default fundmanetal frequency input
                'f0source', 'noisy', ...
                'fs',16e3,... % sampling frequency
                'window', @(N) sqrt(hamming(N,'periodic')), ... % chosen window function
                'xi_ml_min', 0, ... % defines minimum ratio of speech variance ml estimate and noise variance 
                'xi_min', 0, ... % defines minimum ratio of speech and noise variance 
                'zeta_min', 0.001, ... % minimum a posteriori SNR, only important if gain functions other than the wiener filter are applied
                'zeta_max', 1000, ... % maximum a posteriori SNR, only important if gain functions other than the wiener filter are applied
                'G_max', 1, ... 
                'x', [], ... 
                'alpha_B', 0.7, ...
                'f0Estimator','pefac', ...
                'InterpolateNoisePSD', 1 ... % flag that indicates wether the noise PSD is estimated in DSTFT (=1) or PADSTFT (=0).
                ); 

end