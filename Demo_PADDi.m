function Demo_PADDi()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Illustrates two ways to perform speech enhancement via the pitch-adaptive 
% discrete short-time Fourier transform (PADSTFT) proposed in [1] and [4].
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 14.01.2019
% matlab version: 2017b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "A Simple and Effective Framework for A priori SNR estimation", 
%     appear in Proc. ICASSP 2018.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [3] VOICEBOX: MATLAB toolbox for speech processing.
%     Home page: http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html
% [4]  Y. Ephraim and D. Malah, “Speech enhancement using a minimum mean-square
%      error log-spectral amplitude estimator,” IEEE Trans. Acoust., Speech, Signal Pro-
%      cess., vol. 33, no. 2, pp. 443–445, Apr 1985.
% [5]  Y. Ephraim and D. Malah, “Speech enhancement using a minimum-mean square
%      error short-time spectral amplitude estimator,” IEEE Trans. Acoust., Speech, Sig-
%      nal Process., vol. 32, no. 6, pp. 1109–1121, Dec 1984.
% [6]  T. Lotter and P. Vary, “Speech enhancement by MAP spectral amplitude estima-
%      tion using a super-Gaussian speech model,” EURASIP J. on Advances in Signal
%      Processing, no. 7, May 2005.
% [7]  P.J. Wolfe, S.J. Godsill "Efficient alternatives to the Ephraim and
%     Malah suppression rule for audio signal enhancement" (2003), pp. 1043–1051
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Demo_PitchAdaptiveSpeechEnhancement.m illustrates speech enhancement
%     via the PADSTFT.
%     Copyright (C) 2018 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear all

addpath('./Functions/')
addpath(genpath('./ExternalFunctions/')) % these are functions from [4], needed for f0-estimation

% (1) load noisy speech file [2]:
[y, fs]= audioread('./Audio/dr6_fmgd0_sx124_white_16k_10_Noisy.wav');% from [2]

% (2) Select estimator:
Estimator = 'WienerFilter';
% You may select one of the following gain functions by setting Estimator accordingly: 
            % 'WienerFilter'
            % 'MMSESTSA' [5]
            % 'LSA' [4]
            % 'JMAPLV' [6]
            % 'JMAPGW' [7]
            %
            % You can add additional gain functions in the function 'STFT_GetGain.m'
            

% (3) run PADDi [1]:
xhat_PADDi = PADDi(y,...
                'fs',fs,...
                'framelength', 0.032,...
                'Estimator', Estimator); %
            
            
% (4) run DD [5]:
xhat_DD = STFT_SpeechEnhancement(y,...
                                  'fs',fs, ...
                                  'framelength', 0.032, ...
                                  'Estimator', Estimator);  

% (5) plot and play the audiosignals
y = audiosignal(y);
xhat_DD = audiosignal(xhat_DD);
xhat_PADDi = audiosignal(xhat_PADDi);

figure
subplot(221)
y.plot_spectrogram; caxis([-80 -30]); colorbar; title('Noisy')
subplot(222)
xhat_DD.plot_spectrogram; caxis([-80 -30]); colorbar; title('DD')
subplot(224)
xhat_PADDi.plot_spectrogram; caxis([-80 -30]); colorbar; title('PADDi')


y.play; pause(y.siglength)
xhat_DD.play; pause(xhat_DD.siglength)
xhat_PADDi.play; pause(xhat_PADDi.siglength)

end


